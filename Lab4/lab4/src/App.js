import logo from './logo.svg';
import './App.css';

import AboutUsPage from './page/AboutUsPage';
import BMICalPage from './page/BMICalPage';
import Header from './components/Header';
import { Routes, Route } from "react-router-dom";


function App() {
  return (
    <div className="App">
        <Header />
        <Routes>
            <Route path="about" element={
                    <AboutUsPage />
                  } />
            
            <Route path="/" element={
                    <BMICalPage />
                  } />

            
        </Routes>
    </div>
  );
}

export default App;