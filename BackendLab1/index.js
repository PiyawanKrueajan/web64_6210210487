const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
    res.send('Hello World!')
})


app.post('/bmi', (req, res) => {
    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}

    if (!isNaN(weight) && !isNaN(height)) {
        let bmi = (height * height) / weight
        result = {
            "status": 200,
            "bmi": bmi
        }
    } else {

        result = {
            "status": 400,
            "message": "Weight or Height is not a number"
        }
    }

    res.send(JSON.stringify(result))
})


app.get('/triangle', (req, res) => {
    let base = parseFloat(req.query.base)
    let hight = parseFloat(req.query.hight)
    var result = {}

    if (!isNaN(base) && !isNaN(hight)) {
        let triangle = 1 / 2 * base * hight
        result = {
            "status": 200,
            "triangle": triangle
        }
    } else {

        result = {
            "status": 400,
            "message": "Base or Hight is not a number"
        }
    }

    res.send(JSON.stringify(result))
})


app.post('/score', (req, res) => {
    let score = parseFloat(req.query.score)
    var grade = {}

    if (score >= 75) {
        grade = "A"
    } else if (score >= 70) {
        grade = "B+"
    } else if (score >= 65) {
        grade = "B"
    } else if (score >= 60) {
        grade = "C+"
    } else if (score >= 55) {
        grade = "C"
    } else if (score >= 50) {
        grade = "D+"
    } else if (score >= 45) {
        grade = "D"
    } else if (score < 40) {
        grade = "F"
    } else {

        grade = {
            "status": 400,
            "message": "Grade is not a number"
        }
    }

    res.send(JSON.stringify(req.query.name + "GRADE : " + grade))
})


app.get('/hello', (req, res) => {
    res.send('Sawaddee ' + req.query.name)
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})