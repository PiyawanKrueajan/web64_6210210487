import logo from './logo.svg';
import './App.css';

import { Box, AppBar, Toolbar, Typography, Card } from '@mui/material';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';

import { useEffect, useState } from 'react';
import axios from 'axios';
import ReactWeather, { useOpenWeather } from 'react-open-weather';
import YouTube from 'react-youtube';

function App() {
 
	  const [ temperature, setTemperature ] = useState();
	  
	  const weatherAPIBaseUrl = 
		  "https://api.openweathermap.org/data/2.5/weather?";
	  
	  const city = "Songkhla"
	  const apiKey = "f9b550adfa6237a608bcf94ff60af405";
	   
	  const { data, isLoading, errorMessage } = useOpenWeather({
		key: 'f9b550adfa6237a608bcf94ff60af405',
		lat: '7.1182',
		lon: '100.489',
		lang: 'en',
		unit: 'metric', // values are (metric, standard, imperial)
	  });

      // OpenWeatherMap API Key f9b550adfa6237a608bcf94ff60af405 
	  useEffect(  () => {
			setTemperature ("----------");

			axios.get(weatherAPIBaseUrl+"q="+city+"&appid="+apiKey).then ( (response)=> {
			  let data = response.data;
			  let temp = data.main.temp - 273;
			  setTemperature(temp.toFixed(2));
			})
  }
  , [] );

	  return (
	   <Box sx={{ flexGrow : 1, width : "100%"  }}>
		   <AppBar position="static">
			  <Toolbar>
				<Typography variant="h6">
					WeatherApp
				</Typography>
			  </Toolbar>
		   </AppBar>

		   <Box sx = {{ justifyContent : 'center', marginTop : "20px",  width : "100%", display : "flex" }}>
				  <Typography variant="h4">
						 อากาศหาดใหญ่วันนี้
				  </Typography>  
		   </Box>
          
		  <Box sx = {{ justifyContent : 'center', marginTop : "20px",  width : "100%", display : "flex" }}>   
              <Card sx={{ minWidth: 275 }}>
				 <CardContent>
                        <Typography sx={{ fontSize: 14 }} color="text.secondary" >
                            อุณหภูมิหาดใหญ่วันนี้คือ
                        </Typography>
                        
						<Typography variant="h5" component="div">
                            { temperature }
                        </Typography>
                        
						<Typography variant="h5" component="div">
                            องศาเซลเซียส
                        </Typography>
                
				 </CardContent>
                           
              </Card>

              <ReactWeather
				  isLoading={isLoading}
				  errorMessage={errorMessage}
				  data={data}
				  lang="en"
				  locationLabel="Munich"
				  unitsLabels={{ temperature: 'C', windSpeed: 'Km/h' }}
				  showForecast/>

          </Box>   
          
		  <YouTube videoId="CuklIb9d3fI" />
       </Box>
  );
}

export default App;









